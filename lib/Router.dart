import 'package:flutter/material.dart';
import 'package:project_test_imic/Ui/Screen/Order/buynow_screen.dart';

import 'package:project_test_imic/Ui/Screen/carts/Cart_screen.dart';
import 'package:project_test_imic/Ui/Screen/home/Bottombar_home.dart';
import 'package:project_test_imic/Ui/Screen/login/Login_screen.dart';
import 'package:project_test_imic/Ui/Screen/product/product_detail.dart';
import 'package:project_test_imic/Ui/Screen/splash/Splash_Screen.dart';

import 'package:project_test_imic/model/param_argument.dart';

const String initialRoute = SplashScreen.pageName;

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(
          builder: (_) => BottomBar(),
          settings: RouteSettings(name: '/'),
        );
      case CartProduct.pageName:
        return MaterialPageRoute(
            builder: (_) => CartProduct(),
            settings: RouteSettings(name: '/CartProduct'));

      case '/LoginPage':
        return MaterialPageRoute(
            builder: (_) => Login(),
            settings: RouteSettings(name: '/LoginPage'));
      case '/BuyNow':
        return MaterialPageRoute(
            builder: (_) => BuyNow(), settings: RouteSettings(name: '/BuyNow'));
      case '/Splash':
        return MaterialPageRoute(
          builder: (_) => SplashScreen(),
          settings: RouteSettings(name: '/Splash'),
        );
      case ItemDetail.pageName:
        return MaterialPageRoute(
          builder: (_) => ItemDetail(paramArguments: args as ParamArguments),
        );

      default:
        return MaterialPageRoute(
            builder: (_) => Login(), settings: RouteSettings(name: '/'));
    }
  }
}
