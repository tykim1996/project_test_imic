import 'package:flutter/material.dart';

class BuyNow extends StatelessWidget {
  static const String pageName = "/BuyNow";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Thanh toán"),
        ),
        body: Center(
          child: InkWell(
              onTap: () {
                showAlertDialog(context);
              },
              child: Container(
                  width: 200,
                  height: 70,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: Center(
                    child: Text(
                      "Trang thanh toan",
                      style: TextStyle(color: Colors.black, fontSize: 20),
                    ),
                  ))),
        ));
  }
}

showAlertDialog(BuildContext context) {
  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {},
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("My title"),
    content: Text("DAT HANG."),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
