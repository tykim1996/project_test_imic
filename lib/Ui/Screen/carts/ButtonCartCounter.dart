import 'package:flutter/material.dart';
import 'package:project_test_imic/viewmodel/CartsViewModel.dart';
import 'package:provider/provider.dart';

class CartCounter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cartmodel = Provider.of<CartViewModel>(context);
    return InkWell(
      onTap: () => {
        print("click carticon"),
        Navigator.of(context).pushNamed("/CartProduct")
      },
      child: Container(
        child: Row(
          children: [
            Container(
              child: Icon(Icons.shopping_cart),
            ),
            Text(
              "${cartmodel.counterCart.toString()}",
              style: TextStyle(color: Colors.red, fontSize: 17),
            ),
          ],
        ),
      ),
    );
  }
}
