import 'package:flutter/material.dart';
import 'package:project_test_imic/Ui/Screen/carts/ListCarts.dart';
import 'package:project_test_imic/viewmodel/CartsViewModel.dart';
import 'package:provider/provider.dart';

class CartProduct extends StatelessWidget {
  static const String pageName = "/CartProduct";
  @override
  Widget build(BuildContext context) {
    //final cartrmodel = Provider.of<CartsModel>(context);
    final cartrmodel = Provider.of<CartViewModel>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Giỏ hàng",
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        actions: <Widget>[
          Center(
              child: Row(
            children: [
              Container(
                width: 85,
                height: 35,
                margin: EdgeInsets.only(right: 40),
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Center(
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context).pushNamed("/BuyNow");
                    },
                    child: Text(
                      "Mua",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 20),
                child: Text(
                  "${cartrmodel.totalprice}",
                  style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      color: Colors.red),
                ),
              ),
            ],
          ))
        ],
      ),
      body: Container(
        child: ListCarts(),
      ),
    );
  }
}
