import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:project_test_imic/Ui/Screen/product/product_detail.dart';
import 'package:project_test_imic/viewmodel/CartsViewModel.dart';
import 'package:project_test_imic/viewmodel/CouterItemsCart.dart';
import 'package:project_test_imic/model/param_argument.dart';
import 'package:provider/provider.dart';

class CartsItems extends StatefulWidget {
  final dynamic product;
  const CartsItems({Key key, this.product}) : super(key: key);

  @override
  _CartsItemsState createState() => _CartsItemsState();
}

class _CartsItemsState extends State<CartsItems> {
  int total = 1;
  double ktprice = 0;
  bool checkboxvalue = false;
  void increment() {
    setState(() {
      return total++;
    });
  }

  void decrement() {
    setState(() {
      if (total >= 1) {
        return total--;
      }
    });
  }

  @override
  // ignore: must_call_super
  Widget build(BuildContext context) {
    final cartrmodel = Provider.of<CartViewModel>(context);
    final couteritemsmodel = Provider.of<CouterItemsCart>(context);
    couteritemsmodel.set(total);
    ktprice = double.parse(widget.product["price"]);
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15.0),
        border: Border.all(color: Colors.grey),
      ),
      child: Container(
        margin: EdgeInsets.all(3),
        child: Row(
          children: [
            Container(
              width: 20,
              height: 20,
              child: Checkbox(
                value: checkboxvalue,
                onChanged: (bool newvalue) {
                  cartrmodel.chaneCheck(newvalue);
                  checkboxvalue = cartrmodel.chekbox;
                  if (checkboxvalue == true) {
                    print(ktprice);
                    cartrmodel.addTotalprice(ktprice * total);
                  }
                  if (checkboxvalue == false) {
                    print(ktprice);
                    cartrmodel.deTotalprice(ktprice * total);
                  }
                },
              ),
            ),
            Container(
              width: 100,
              height: 120,
              margin: EdgeInsets.only(right: 5),
              child: Image.asset(
                "assets/images/${widget.product["img"]}",
                fit: BoxFit.cover,
              ),
            ),
            Flexible(
              child: Container(
                width: 140,
                child: InkWell(
                  onTap: () {
                    //truyen param tu listcart quay lai -> chi tiet
                    Navigator.of(context).pushNamed(
                      ItemDetail.pageName,
                      arguments: ParamArguments(param: widget.product),
                    );
                  },
                  child: Column(
                    children: [
                      Text(
                        "${widget.product["name"]}",
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        "${widget.product["price"]}",
                        style: TextStyle(
                            fontSize: 19,
                            fontWeight: FontWeight.bold,
                            color: Colors.red),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Column(
              children: [
                Row(
                  children: [
                    InkWell(
                      onTap: () => {
                        decrement(),
                        if (total == 0)
                          {
                            cartrmodel.removeCarts(widget.product),
                            //xoa 1 san pham cap nhat lai total
                            total += 1,
                            Scaffold.of(context).showSnackBar(
                              SnackBar(
                                content: Text("Xoa hàng thành công !"),
                              ),
                            ),
                            cartrmodel.decrement(),
                          },
                        if (checkboxvalue == true)
                          {
                            cartrmodel.deTotalprice(ktprice),
                          }
                      },
                      child: Container(
                        margin: EdgeInsets.only(bottom: 14, left: 10),
                        child: Icon(Icons.minimize_outlined),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 16),
                      child: Text(
                        // ignore: unnecessary_brace_in_string_interps
                        "${couteritemsmodel.total}",
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.normal,
                            color: Colors.redAccent),
                      ),
                    ),
                    InkWell(
                      onTap: () => {
                        increment(),
                        if (checkboxvalue == true)
                          {
                            cartrmodel.addTotalprice(ktprice),
                          }
                      },
                      child: Container(
                        margin: EdgeInsets.only(left: 20),
                        child: Icon(Icons.add),
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(left: 10),
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context).pushNamed("/BuyNow");
                    },
                    child: Container(
                      width: 70,
                      height: 25,
                      decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      child: Center(
                        child: Text(
                          "Mua ngay",
                          style: TextStyle(
                              fontSize: 10,
                              fontStyle: FontStyle.normal,
                              color: Colors.black),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
