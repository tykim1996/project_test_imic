import 'package:flutter/material.dart';
import 'package:project_test_imic/Ui/Screen/carts/Carts_Items.dart';
import 'package:project_test_imic/viewmodel/CartsViewModel.dart';
import 'package:provider/provider.dart';

class ListCarts extends StatefulWidget {
  @override
  _ListCartsState createState() => _ListCartsState();
}

class _ListCartsState extends State<ListCarts>
    with AutomaticKeepAliveClientMixin {
  @override
  // ignore: must_call_super
  Widget build(BuildContext context) {
    final cartrmodel = Provider.of<CartViewModel>(context);
    return Container(
        child: Column(
      children: [
        Expanded(
          child: ListView.builder(
            shrinkWrap: true,
            padding: const EdgeInsets.all(8),
            itemCount: cartrmodel.carts.length,
            itemBuilder: (BuildContext context, int index) {
              return CartsItems(product: cartrmodel.carts[index]);
            },
          ),
        ),
      ],
    ));
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
