import 'package:flutter/material.dart';
import 'package:project_test_imic/Ui/Screen/carts/Cart_screen.dart';
import 'package:project_test_imic/Ui/Screen/home/home_screen.dart';
import 'package:project_test_imic/Ui/Screen/user/Profile_screen.dart';
import 'package:project_test_imic/api/ALBUM/NewApi_Screns.dart';

class BottomBar extends StatefulWidget {
  static const String pageName = "/";
  const BottomBar({Key key}) : super(key: key);
  @override
  _BottomBarState createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  int _selectedIndex = 0;
  int _widgetIndex = 0;
  final widgetOptions = <Widget>[
    MyHomePage(),
    CartProduct(),
    RestApi(),
    ProfilePage(),
  ];
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      _widgetIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _widgetIndex,
        children: widgetOptions,
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.red,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
            backgroundColor: Colors.grey.shade400,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart_outlined),
            label: 'Cart',
            backgroundColor: Colors.grey.shade400,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.chrome_reader_mode),
            label: 'Message',
            backgroundColor: Colors.grey.shade400,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.supervised_user_circle),
            label: 'Settings',
            backgroundColor: Colors.grey.shade400,
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.black,
        onTap: _onItemTapped,
      ),
    );
  }
}
