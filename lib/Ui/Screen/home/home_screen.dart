import 'package:flutter/material.dart';
import 'package:project_test_imic/Ui/Screen/carts/ButtonCartCounter.dart';
import 'package:project_test_imic/Ui/Screen/home/tabbar_home.dart';
import 'package:project_test_imic/Ui/Widget/homewidget.dart';

class MyHomePage extends StatefulWidget {
  static const String pageName = "/MyHomePage";

  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with ListItem {
  @override
  Widget build(BuildContext context) {
    // final coutermodel = Provider.of<CounterModel>(context);
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: builSearchtab(context),
                ),
                SizedBox(
                  width: 30,
                ),
                //ICON GIO HANG
                CartCounter(),

                SizedBox(
                  width: 30,
                ),
              ],
            ),
            //builHeaderText(context),
            // builListItem(context),
            TabarWidget(),
          ],
        ),
      ),
    );
  }
}
