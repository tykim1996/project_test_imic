import 'package:flutter/material.dart';
import 'package:project_test_imic/Ui/Screen/product/product_screen.dart';
import 'package:project_test_imic/Ui/Widget/homewidget.dart';

class TabarWidget extends StatelessWidget with ListItem {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: DefaultTabController(
        length: 4,
        child: Column(
          children: [
            TabBar(
              isScrollable: true,
              tabs: [
                Tab(
                  child: Text(
                    "Sport",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Tab(
                  child: Text(
                    "Patin",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Tab(
                  child: Text(
                    "Soccor",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Tab(
                  child: Text(
                    "Walking",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
            Expanded(
              child: TabBarView(
                children: [
                  GirdViewdata(),
                  GirdViewdata(),
                  GirdViewdata(),
                  GirdViewdata(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
