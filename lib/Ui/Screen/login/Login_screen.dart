import 'package:flutter/material.dart';
import 'package:project_test_imic/Ui/Screen/home/Bottombar_home.dart';
import 'package:project_test_imic/model/param_argument.dart';
import 'package:project_test_imic/Ui/Screen/product/product_detail.dart';

class Login extends StatefulWidget {
  static const String pageName = "/LoginPage";
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 20),
            child: Text(
              "Đăng Nhập",
              style: TextStyle(
                  fontSize: 60,
                  color: Colors.blueGrey,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: TextField(
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  borderSide: BorderSide(color: Colors.black, width: 2),
                ),
                hintText: "Nhập tên tài khoản",
              ),
              style:
                  TextStyle(fontSize: 15.0, height: 2.0, color: Colors.black),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10, left: 20, right: 20),
            child: TextField(
              obscureText: true,
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  borderSide: BorderSide(color: Colors.black, width: 2),
                ),
                hintText: "Nhập mật khẩu",
              ),
              style:
                  TextStyle(fontSize: 15.0, height: 2.0, color: Colors.black),
            ),
          ),
          Container(
            width: 250,
            height: 70,
            margin: EdgeInsets.only(top: 10, left: 20, right: 20),
            child: Row(
              children: [
                RaisedButton(
                  color: Colors.white54,
                  child: Text(
                    "Login",
                    style: TextStyle(
                        fontSize: 15.0,
                        height: 2.0,
                        color: Colors.blueAccent,
                        fontWeight: FontWeight.bold),
                  ),
                  onPressed: () => {
                    Navigator.of(context).pushNamed(BottomBar.pageName),
                  },
                ),
                SizedBox(
                    width: 30,
                    child: Center(
                      child: Text("or"),
                    )),
                InkWell(
                  child: Text(
                    "Create Account",
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.blue),
                  ),
                  onTap: () => {
                    Navigator.of(context).pushNamed(
                      ItemDetail.pageName,
                      arguments: ParamArguments(
                          id: '1', param: {'title': 'cate product'}),
                    ),
                  },
                )
              ],
            ),
          ),
          Container(
            height: 60,
            width: 300,
            decoration: BoxDecoration(
              color: Colors.blueGrey,
              borderRadius: BorderRadius.circular(16.0),
            ),
            child: Center(
              child: InkWell(
                child: Text(
                  "Sigin With Google",
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.lightBlueAccent),
                ),
                onTap: () => {
                  Navigator.of(context).pushReplacementNamed('/'),
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
