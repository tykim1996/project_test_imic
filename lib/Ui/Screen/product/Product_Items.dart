import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:project_test_imic/Ui/Widget/homewidget.dart';
import 'package:project_test_imic/viewmodel/CartsViewModel.dart';
import 'package:provider/provider.dart';

class ProductItems extends StatefulWidget with ListItem {
  final dynamic product;

  const ProductItems({Key key, this.product}) : super(key: key);

  @override
  _ProductItemsState createState() => _ProductItemsState();
}

class _ProductItemsState extends State<ProductItems> {
  @override
  Widget build(BuildContext context) {
    // final coutermodel = Provider.of<CounterModel>(context);
    final cartmodel = Provider.of<CartViewModel>(context);
    return Container(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey),
          color: Colors.white10,
        ),
        //color: Colors.grey,
        child: Column(children: [
          Container(
            width: 120,
            height: 100,
            child: Image.asset(
              "assets/images/${widget.product["img"]}",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            child: Column(
              children: [
                Text(
                  "${widget.product["name"]}",
                  style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic,
                      color: Colors.cyan),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      "${widget.product["price"]}",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Container(
                      child: InkWell(
                        onTap: () {
                          bool kt = true;
                          //san pham chi khac id
                          for (int i = 0; i < cartmodel.carts.length; i++) {
                            if (cartmodel.carts[i]["id"].toString() ==
                                widget.product["id"].toString()) {
                              // cartitemsrmodel.increment();
                              kt = false;
                            }
                          }
                          if (kt == true) {
                            cartmodel.increment();
                            cartmodel.addCarts(widget.product);
                            Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text("Thêm giỏ hàng thành công !")));
                          }
                          if (kt == false) {
                            print("kt false");
                            Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text("Sản phẩm đã tồn tại !")));
                          }
                        },
                        child: Icon(
                          Icons.add,
                          color: Colors.indigoAccent,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    )
                  ],
                ),
              ],
            ),
          ),
        ]),
      ),
    );
  }
}
