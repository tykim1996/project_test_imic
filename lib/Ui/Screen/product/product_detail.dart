import 'package:flutter/material.dart';
import 'package:project_test_imic/Ui/Screen/carts/ButtonCartCounter.dart';
import 'package:project_test_imic/viewmodel/CartsViewModel.dart';
import 'package:project_test_imic/viewmodel/CouterItemsCart.dart';
import 'package:project_test_imic/model/param_argument.dart';
import 'package:provider/provider.dart';

class ItemDetail extends StatefulWidget {
  final ParamArguments paramArguments;
  static const String pageName = "/ItemDetail";

  const ItemDetail({Key key, this.paramArguments}) : super(key: key);

  @override
  _ItemDetailState createState() => _ItemDetailState();
}

class _ItemDetailState extends State<ItemDetail> {
  dynamic ojb;

  @override
  Widget build(BuildContext context) {
    final cartrmodel = Provider.of<CartViewModel>(context);
    final cartitemsrmodel = Provider.of<CouterItemsCart>(context);
    ojb = widget.paramArguments.param;
    return Scaffold(
      appBar: AppBar(
        title: Text("Sản phẩm chi tiết"),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Stack(
                children: [
                  Container(
                    width: 370,
                    height: 250,
                    margin: EdgeInsets.only(top: 50, left: 5, right: 5),
                    child: Image.asset(
                      "assets/images/${ojb["img"].toString()}",
                      fit: BoxFit.cover,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 20, top: 40),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        CartCounter(),
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                child: Row(
                  children: [
                    Container(
                      width: 300,
                      height: 70,
                      margin: EdgeInsets.only(left: 20),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Text(
                                "${ojb["name"].toString()}",
                                style: TextStyle(
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.normal,
                                    color: Colors.blue),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                "Giá : ",
                                style: TextStyle(
                                    fontSize: 17,
                                    fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.normal,
                                    color: Colors.black),
                              ),
                              Text(
                                "${ojb["price"].toString()}",
                                style: TextStyle(
                                    fontSize: 17,
                                    fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.normal,
                                    color: Colors.red),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                "Sizes : ",
                                style: TextStyle(
                                    fontSize: 17,
                                    fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.normal,
                                    color: Colors.black),
                              ),
                              Text(
                                "${ojb["sizes"].toString()}",
                                style: TextStyle(
                                    fontSize: 17,
                                    fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.normal,
                                    color: Colors.black),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () => {
                      print("mua ngay + add total"),
                      cartrmodel.addCarts(ojb),
                      cartrmodel.increment(),
                      Navigator.of(context).pushNamed("/BuyNow")
                    },
                    child: Container(
                        margin: EdgeInsets.only(left: 20),
                        width: 140,
                        height: 50,
                        decoration: BoxDecoration(
                          color: Colors.orange,
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        child: Center(
                          child: Text(
                            "Mua ngay",
                            style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                                fontStyle: FontStyle.normal,
                                color: Colors.black),
                          ),
                        )),
                  ),
                  InkWell(
                    onTap: () {
                      //Provider add cart
                      bool kt = true;
                      //bắt theo tên thayvì Id
                      for (int i = 0; i < cartrmodel.carts.length; i++) {
                        if (cartrmodel.carts[i]["name"].toString() ==
                            ojb["name"].toString()) {
                          // cartitemsrmodel.increment();
                          kt = false;
                          return;
                        }
                      }
                      if (kt == false) {
                        cartitemsrmodel.increment();
                      }
                      if (kt == true) {
                        cartrmodel.increment();
                        cartrmodel.addCarts(ojb);
                      }
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 20),
                      width: 140,
                      height: 50,
                      decoration: BoxDecoration(
                        color: Colors.orange,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Center(
                        child: Text(
                          "Thêm giỏ hàng",
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.normal,
                              color: Colors.black),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                  height: 120,
                  margin: EdgeInsets.only(top: 20),
                  child: Text(
                    "==== Mô tả === ",
                    style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.normal,
                        color: Colors.black),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
