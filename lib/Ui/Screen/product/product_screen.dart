import 'package:flutter/material.dart';
import 'package:project_test_imic/Ui/Screen/product/Product_Items.dart';
import 'package:project_test_imic/Ui/Screen/product/product_detail.dart';
import 'package:project_test_imic/model/param_argument.dart';
import 'package:project_test_imic/product_data.dart';

class GirdViewdata extends StatefulWidget {
  static const String pageName = "/GirdViewdata";
  @override
  _GirdViewdataState createState() => _GirdViewdataState();
}

class _GirdViewdataState extends State<GirdViewdata>
    with AutomaticKeepAliveClientMixin {
  @override
  // ignore: must_call_super
  Widget build(BuildContext context) {
    return GridView.count(
      shrinkWrap: true,
      primary: false,
      padding: const EdgeInsets.all(8),
      crossAxisCount: 2,
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      children: List.generate(products.length, (index) {
        return InkWell(
          onTap: () => {
            print(products[index]),
            Navigator.of(context).pushNamed(
              ItemDetail.pageName,
              arguments: ParamArguments(param: products[index]),
            ),
          },
          child: ProductItems(
            product: products[index],
          ),
        );
      }),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
