import 'package:flutter/material.dart';
import 'package:project_test_imic/services/init.dart';

class SplashScreen extends StatefulWidget {
  static const String pageName = "/Splash";
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  void _loadData() async {
    await Init.initialize();
    try {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/LoginPage', (route) => false);
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    _loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber,
      body: Container(child: _buildLoadingScreen()),
    );
  }

  Widget _buildLoadingScreen() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Loading",
            style: TextStyle(
              fontSize: 32,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 20),
          CircularProgressIndicator()
        ],
      ),
    );
  }
}
