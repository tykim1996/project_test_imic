import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project_test_imic/Ui/Screen/product/Product_Items.dart';
import 'package:project_test_imic/product_data.dart';

class ListItem {
  Widget builIconSearch(BuildContext context) {
    return Container(
      child: Icon(Icons.search),
    );
  }

  Widget builSearchtab(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      width: 100,
      height: 40,
      child: TextField(
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.black, width: 1),
          ),
          icon: Icon(Icons.search),
          hintText: "Tìm kiếm",
        ),
        style: TextStyle(fontSize: 10.0, height: 2.0, color: Colors.black),
      ),
    );
  }

  Widget builIconCart(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Container(
            child: Icon(Icons.shopping_cart),
          ),
          Text(
            "so luong san pham",
            style: TextStyle(color: Colors.red),
          ),
        ],
      ),
    );
  }

  Widget builHeaderText(BuildContext context) {
    return Text(
      "Giày Sport",
      style: TextStyle(
          fontSize: 20,
          color: Colors.black,
          fontStyle: FontStyle.italic,
          fontWeight: FontWeight.bold),
    );
  }

  Widget builListItem(BuildContext context) {
    return GridView.count(
      shrinkWrap: true,
      primary: false,
      padding: const EdgeInsets.all(8),
      crossAxisCount: 2,
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      children: List.generate(products.length, (index) {
        return ProductItems(
          product: products[index],
        );
      }),
    );
  }
}
