import 'package:flutter/material.dart';
import 'package:project_test_imic/model/Album.dart';
import 'package:project_test_imic/api/ALBUM/getAPI.dart';

class RestApi extends StatefulWidget {
  @override
  _RestApiState createState() => _RestApiState();
}

class _RestApiState extends State<RestApi> {
  Future<List<Album>> futureAlbum;
  @override
  void initState() {
    super.initState();
    futureAlbum = getAPI().fetchAlbum();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: FutureBuilder<List<Album>>(
        future: futureAlbum,
        builder: (context, snapshot) {
          List<Album> data = snapshot.data;
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: data.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  height: 75,
                  color: Colors.white,
                  child: Center(
                    child: Text(data[index].title),
                  ),
                );
              },
            );
          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }
          return CircularProgressIndicator();
        },
      )),
    );
  }
}
