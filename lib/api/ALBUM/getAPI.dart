import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:project_test_imic/model/Album.dart';

// ignore: camel_case_types
class getAPI {
  Future<List<Album>> fetchAlbum() async {
    final response =
        await http.get('https://jsonplaceholder.typicode.com/albums');
    List<Album> album = [];

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      dynamic jsonraw = json.decode(response.body);
      List<dynamic> data = jsonraw;
      data.forEach((p) {
        album.add(Album.fromJson(p));
      });
      return album;
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }
}
