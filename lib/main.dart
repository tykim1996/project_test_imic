import 'package:flutter/material.dart';
import 'package:project_test_imic/Router.dart';
import 'package:project_test_imic/viewmodel/CartsViewModel.dart';
import 'package:project_test_imic/viewmodel/CouterItemsCart.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<CartViewModel>(
          create: (context) => CartViewModel(),
        ),
        ChangeNotifierProvider<CouterItemsCart>(
          create: (context) => CouterItemsCart(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        //home: BottomBar(),
        initialRoute: initialRoute,
        onGenerateRoute: RouteGenerator.generateRoute,
      ),
    );
  }
}
