class Init {
  static Future initialize() async {
    await _registerServices();
    await _loadSettings();
  }

  static _registerServices() async {
    print("Loading services 1");
    await Future.delayed(Duration(seconds: 1));
    print("Loading services 1 done");
  }

  static _loadSettings() async {
    print("Loading services 2");
    await Future.delayed(Duration(seconds: 1));
    print("Loading services 2 done");
  }
}
