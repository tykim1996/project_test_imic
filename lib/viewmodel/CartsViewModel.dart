import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';

class CartViewModel extends ChangeNotifier {
  int _counterCart = 0;
  bool _chekbox = false;
  double _totalPrice = 0;
  List<dynamic> _carts = [];
  List<dynamic> get carts {
    return this._carts;
  }

//  cart------------------------
  void addCarts(dynamic product) {
    this._carts.add(product);
    notifyListeners();
  }

  void removeCarts(dynamic product) {
    this._carts.remove(product);
    notifyListeners();
  }

  // cart===========================
//counterCart==============
  int get counterCart {
    return _counterCart;
  }

  int set(int number) {
    return _counterCart = number;
  }

  void increment() {
    _counterCart++;
    notifyListeners();
  }

  void decrement() {
    if (_counterCart > 0) {
      _counterCart--;
    }
    notifyListeners();
  }
  //countercart=================================

  //totalprice====================
  double get totalprice => _totalPrice;
  double settertotal(double price) {
    return _totalPrice = price;
  }

  void addTotalprice(double price) {
    _totalPrice = _totalPrice + price;
    notifyListeners();
  }

  void deTotalprice(double price) {
    _totalPrice -= price;
    notifyListeners();
  }
  //totalprice==================

  //checkbox===================
  bool get chekbox => _chekbox;
  bool settercheckbox(bool ktcheck) {
    return _chekbox = ktcheck;
  }

  void chaneCheck(bool newcheckboxvalue) {
    _chekbox = newcheckboxvalue;
    notifyListeners();
  }
}
