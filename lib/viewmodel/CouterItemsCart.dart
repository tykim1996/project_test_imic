import 'package:flutter/cupertino.dart';

class CouterItemsCart extends ChangeNotifier {
  int _total = 1;
  int get total {
    return _total;
  }

  int set(int number) {
    return this._total = number;
  }

  void increment() {
    _total++;
    notifyListeners();
  }

  void decrement() {
    if (_total > 0) {
      _total++;
      notifyListeners();
    }
  }
}
